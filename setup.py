#!/usr/bin/python

from setuptools import setup
setup(name='seanoodle',
    version='0.1.0',    
    description='A marine router',
    url='https://gitlab.com/Houkime/sea-noodle',
    include_package_data=True,
    author='Houkime',
    author_email = 'houkime@protonmail.com',
    license = 'MIT',
    packages = ['seanoodle'],
    install_requires = [
                      'pathlib',
                      'geopandas',
                      'great_circle_calculator',
                      'gpxpy',
                      'shapely',
                      ],

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: MIT License',  
        'Operating System :: POSIX :: Linux',        
        'Programming Language :: Python :: 3.5',
    ],
)