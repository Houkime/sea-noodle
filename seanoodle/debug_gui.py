#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Debug GUI for seanoodle
Note that it has a bit of additional dependencies
"""

############################### IMPORTS ###################################

import seanoodle as sn

#### GUI deps
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib
import threading
import time
from matplotlib.backends.backend_gtk3agg import (FigureCanvasGTK3Agg as
                                                 FigureCanvas)

############################### CONSTANTS ##################################

### control flow for GUI
STOP = False
SEMAPHORE = True

############################## WINDOW FOR GUI ##############################


class MyWindow(Gtk.Window):
    """
    Window for debug GUI
    """
    def __init__(self, figure):
        Gtk.Window.__init__(self, title="Hello World")
        self.drawer = FigureCanvas(figure)
        self.add(self.drawer)

################# Functions for Debug Gui ###############################

########## these use global database connection

def dump_point_set_to_line (point_status, line):
    points = list(
        con.execute(
            '''SELECT x, y
                                FROM points
                                WHERE status == ?''', (point_status, )))
    points_2_line(points, line)

def visualise_state(edge_line, stale_line, land_line, candidate_line):
    """
    Tap into the database and display different points with different colors.
    Lines are colored lines that are initialised only once and later just updated by this function
    """
    
    line_dict = {
            sn.PointStatus.EDGE: edge_line,
            sn.PointStatus.STALE: stale_line,
            sn.PointStatus.GROUND: land_line,
            sn.PointStatus.CANDIDATE: candidate_line,
            }
    
    for status in line_dict.keys():
        dump_point_set_to_line (status, line_dict[status])

def callback(step):
    """
    Gets periodically executed in the main thread through Glib idling.
    Operations with inital database connection cannot be done from another thread!
    """
    global STOP
    global SEMAPHORE
    print("updating")
    if SEMAPHORE:
        SEMAPHORE = False
        res = sn.route_step(start_point, end_point, con, step)
        visualise_state(edge_line, inactive_line, land_line, candidate_line)
        if res == 1:
            route = sn.find_winner_route(end_point, con, step)
            STOP = True
            points_2_line(route, route_line)
            sn.simplify_route(route, STEP)
            points_2_line(route, simplified_route_line)
            
        win.drawer.draw()
        SEMAPHORE = True
    else:
        print("not doing anything, semaphore")


def example_target():
    """
    An auxiliary thread which inserts update and visualisation calls into glib idling thing.
    Needs to be another thread from main because otherwise you can't inject anything to gtk looping process.
    Injecting into a loop is needed for semi-real time visualisation.
    """
    global STOP
    if not STOP:
        print("thread started")
        while True:
            if not STOP:
                if SEMAPHORE:
                    GLib.idle_add(callback, STEP)
                time.sleep(0.01)
                #time.sleep(0.2)
            else:
                break


def points_2_line(array, line):  
    """
    Array is of [lat,long]. Line is an existing line to be modified
    """
    
    line.set_data([x[0] for x in array], [x[1] for x in array])



############### Graphical Debugging Interface #####################
if __name__ == '__main__':

    STEP = 0.02

    ########### CREATE DATABASE FOR POINTS
    con = sn.init_db()

    ######### plot geopandas plot
    a = sn.data.plot()

    ### setting a testing viewport
    MIN_Y = 50
    MAX_Y = 52

    MIN_X = 155
    MAX_X = 158

    a.set_ylim(MIN_Y, MAX_Y)
    a.set_xlim(MIN_X, MAX_X)

    ### Preparing lines for data.
    
    ## They will be live updated -- intial data does not matter. 
    ## Change line styling here.

    land_line = a.plot([152.5], [51],
                       'o',
                       markersize=3,
                       color=(0.9, 0, 0.9),
                       alpha=0.8)[0]  # plot() returns an array
    edge_line = a.plot([152.5], [51],
                       'o',
                       markersize=3,
                       color=(0, 0.9, 0),
                       alpha=0.8)[0]
    inactive_line = a.plot([152.5], [51],
                           'x',
                           markersize=3,
                           color=(0.9, 0.9, 0.9),
                           alpha=0.8)[0]
    endpoints_line = a.plot([152.5], [51],
                            'o',
                            markersize=5,
                            color=(0.9, 0, 0),
                            alpha=0.8)[0]
    route_line = a.plot([152.5], [51],
                        'o',
                        markersize=4,
                        color=(0.9, 0.9, 0),
                        alpha=0.8)[0]
    candidate_line = a.plot([152.5], [51],
                            'o',
                            markersize=2,
                            color=(0, 0.9, 0.9),
                            alpha=0.8)[0]
    simplified_route_line = a.plot([0], [0],
                                   'o',
                                   markersize=4,
                                   color=(0.9, 0, 0.9),
                                   alpha=0.8)[0]

    ### 2 points to route between just for tests. Feel free to change them
    start_point = [155, 50]
    end_point = [157, 51]

    points_2_line([start_point, end_point], endpoints_line)

    ### Priming a routing process
    sn.add_to_edge(start_point, -1, end_point, con)

    ### Creating a window with an updateable matplotlib graph inside.
    
    ## From geopandas we receive only Axes of mpl plot. 
    ## But to embed into gtk we need a parent Figure object.
    f = a.figure  

    win = MyWindow(f)
    win.connect("delete-event", Gtk.main_quit)
    win.set_title("SeaNoodle - DEBUG")
    win.show_all()

    ### Starting a thread for visual live-updates
    thread = threading.Thread(target=example_target)
    thread.daemon = True
    thread.start()

    ### Transferring control to main gtk loop
    Gtk.main()
        