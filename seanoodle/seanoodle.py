#!/usr/bin/python3
"""

A routing engine that outputs a marine route between arbitrary A and B points.
With respect to the coastline and spherical geometry.
It uses A* search algorithm and sqlite to manage info on tons of points.

"""

############################ IMPORTS #############################

from pathlib import Path
import geopandas
import great_circle_calculator.great_circle_calculator as gcc
from gpxpy import gpx as GPX
from shapely.geometry import Point
from math import radians, inf  #asin, sin, cos, sqrt, pi, inf, atan
import sqlite3
from enum import IntEnum, auto


############################## CONSTANTS ###################################

EARTH_RADIUS = 6371  #in km

CAN_CREATE_MULTIPLE_CANDIDATES = True  ##Can be turned off for a bit performance. potentially sacrificing some accuracy.
vertex_count = 0  ###TODO: potentially thread-unsafe?

TOLERANCE = 0.0001
STEP_DETECTION_WIDENING = 1.1

############################## AUX ENUMS ####################################


class PointStatus(IntEnum):
    """
    Types of points we have.
    """
    ## An active point which is iterated over and that can be expanded each cycle
    EDGE = auto()
    
    ## Stale points are not iterated over and cannot be expanded.
    STALE = auto()  
    
    ## Ground point is a special inactive point which corresponds to shore land.
    GROUND = auto()
    
    ## Candidates are otentuial new edgepoints expanded from old ones.
    CANDIDATE = auto()  


class FieldIndex(IntEnum):
    """
    To not keep in mind where fields (though one can unload from sql as dict as alternatve)
    """
    X = 0  # longitude in degrees
    Y = 1  # latitude in degrees
    INDEX = 2  # an unique number representing a point. Makes pointers possible
    PREVIOUS = 3  # a pointer to the point which this point expanded from
    HEURISTICS = 4  # a measure of maximum potential coolness of the trajectory which contains this point. Lower is better.
    STATUS = 5  # See PointStatus
    LENGTH = 6  # A length of a route to this point. Coolness depends on it.^_^


################################## FUNCTIONS ##############################3

################### DATABASE INTERFACE ##################################


def init_db():
    """
    Initialise a database.
    Returns a new connection. 
    """
    con = sqlite3.connect(":memory:")
    con.execute('''CREATE TABLE points
            (x float, y float, ind int, previous int, heuristics float, status int, length float)'''
                )
    return con


def point_from_index(index, con):
    """
    get a _limited_ info on a point based on its index. 
    """
    #print (index)
    res = list(
        con.execute(
            '''SELECT x,y,previous
                            FROM points
                            WHERE ind == ?
                            ''', (index, )))
    #print("point from index", res)
    return [[res[0][FieldIndex.X], res[0][FieldIndex.Y]], res[0][-1]]


def inactivate(index, con):
    """
    make a point stale.
    stale points are not iterated over and cannot be expanded
    """
    con.execute(
        '''UPDATE points
                SET status = ?
                WHERE ind = ?''', (PointStatus.STALE, index))


def add_to_edge(point, previous_index, target_point, con):
    """
    used for priming the process.
    """
    global vertex_count
    con.execute("INSERT INTO points VALUES (?,?,?,?,?,?,?)",
                (point[0], point[1], vertex_count, previous_index,
                 geo_distance(point, target_point), PointStatus.EDGE, 0.0))
    vertex_count += 1


def add_to_ground(x, y, con):
    global vertex_count
    con.execute("INSERT INTO points VALUES (?,?,?,?,?,?,?)",
                (x, y, vertex_count, 0, 0, PointStatus.GROUND, 0))
    vertex_count += 1


def add_candidate(x, y, pre_index, pre_x, pre_y, pre_length, target_point,
                  con, travelled_factor=1):
    global vertex_count
    length = pre_length + geo_distance((x, y), (pre_x, pre_y))
    heuristics = get_heuristic(x, y, length, target_point, travelled_factor)
    #    print(length)
    #    print(heuristics)
    con.execute("INSERT INTO points VALUES (?,?,?,?,?,?,?)",
                (x, y, vertex_count, pre_index, heuristics,
                 PointStatus.CANDIDATE, length))
    vertex_count += 1


def get_edgepoints_iterator(con):
    """
    get an iterator from a database which allows to make 'for' loops over edgepoints
    """
    return con.execute(
        '''SELECT x, y, ind, length
                                FROM points
                                WHERE status == ?
                                ORDER BY heuristics
                                ''', (PointStatus.EDGE, ))


def get_known_neighbors(tuple_fromdatabase, con,
                        step):  #account for inactive because they are ground?
    """
    get all known points that are geometrically near this point.
    SQL here is needed for fast searching through a potentially large amount of points.
    """

    known_neighbors = con.execute(
        '''SELECT x, y, ind, previous, heuristics, status
                            FROM points
                            WHERE x BETWEEN ? AND ? AND y BETWEEN ? AND ?  ''',
        (  #PointStatus.CANDIDATE,
            tuple_fromdatabase[FieldIndex.X] - step * STEP_DETECTION_WIDENING,
            tuple_fromdatabase[FieldIndex.X] + step * STEP_DETECTION_WIDENING,
            tuple_fromdatabase[FieldIndex.Y] - step * STEP_DETECTION_WIDENING,
            tuple_fromdatabase[FieldIndex.Y] + step * STEP_DETECTION_WIDENING))
    return list(known_neighbors)


################## UTILS ##################################################


def is_seapoint(shapely_point, data):
    """
    checks if a point is on sea (true) or on land
    """
    if data.intersects(shapely_point).any():
        return True
    else:
        return False


def geo_distance(point1, point2):
    """
    distance between 2 points (x,y) in km
    """
    return gcc.distance_between_points(point1, point2) / 1000.0


def angle_difference(a, b, full_circ=360.):
    """
    return a difference between 2 angles considering circularity of a value. 
    """
    change = a - b
    if abs(a - b) == inf:
        return 0.0

    if change > full_circ / 2.:
        change = change - full_circ
    if change < -full_circ / 2.:
        change = change + full_circ
    return change


def tuple_2_shapely(point):
    """
    make a shapely point out of tuple or an array. Probably redundant 
    """
    return Point(point[0], point[1])


def waypoints_between(point1, point2, n_points):
    """
    get n_points of Great Circle points between point 1 and point2.
    Points are [x,y] or (x,y)
    """
    step = 1.0 / n_points
    #we don't need starting and ending points
    return [
        gcc.intermediate_point(point1, point2, (step) * (n))
        for n in range(1, n_points + 1)
    ]


###################### SIMPLIFICATION #####################################


def has_los(point1, point2, minimum_land_thickness_m=1000):
    """
    Check if there is land blocking a straight way between points.
    Returns true if there is line of sight and false otherwise.
    Points are either tuples or arrays of 2.
    """

    dist = gcc.distance_between_points(point1, point2)
    n_points = int(dist // minimum_land_thickness_m)
    print("distance and minimum thickness are", dist, minimum_land_thickness_m)
    print("n_points =", n_points)
    if n_points < 1:
        print("has los")
        return True

    answerlist = [
        not is_seapoint(tuple_2_shapely(point), data)
        for point in waypoints_between(point1, point2, n_points)
    ]

    print(answerlist)

    if not any(answerlist):
        print("has los")
        return True
    else:
        print("no los")
        return False


def simplify_route(route, grid_step):
    """
    destructively simplifies route
    is not supposed to be able to remove start or end points
    also one point cannot be in the route twice (this though is guaranteed to not be the case by A*)
    """
    print("called simplifier")
    
    ## If 2 nodes are reducible in a row this doesn't really mean that you can cut them both out!
    max_deletions_in_row = 1 
    
    ## Declaring it out of the loop for it to persist
    simplifications_made = 0
    
    ## It can be modified to cut out big objects also and dynamically increase this value. 
    ## But this will need n**2 operation
    step = 2
    
    while True:
        simplifications_made = 0
        queued_deletions = []
        deletions_in_row = 0
        for i in range(len(route) - step):
            if has_los(route[i], route[i + step],
                       radians(grid_step) * EARTH_RADIUS * 1000):
                if deletions_in_row < max_deletions_in_row:
                    ## TODO: generalize on step !=2 if needed
                    queued_deletions.append(route[i + 1])
                    
                    simplifications_made += 1
                    deletions_in_row += 1
                else:
                    deletions_in_row = 0
        print("about to make", len(queued_deletions), "deletes")
        for p in queued_deletions:
            route.remove(p)
        print("route length afterwards", len(route))
        if simplifications_made == 0:
            print("no further simplifications possible, exiting")
            break
    #return route


####################### ROUTING #############################################


def route_step(start_point, end_point, con, step, travelled_factor = 1):

    for edgepoint in get_edgepoints_iterator(con):
        double_neighbors = False
        known_neighbors = get_known_neighbors(edgepoint, con, step)
        __active = False
        print(edgepoint)
        #print(edgepoint[FieldIndex.X],edgepoint[FieldIndex.X])
        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                x = round(edgepoint[FieldIndex.X] + dx * step, 5)
                if x>180:
                    x = -360 + x
#                        if not double_neighbors
#                            known_neighbors = known_neighbors +
                if x<-180:
                    x = 360 + x
                         
                    
                y = round(edgepoint[FieldIndex.Y] + dy * step, 5)
                print("checking", x, y)

                collisions = [
                    n for n in known_neighbors
                    if abs(n[FieldIndex.X] - x) < 0.0001  #the thing drifts!!!
                    and abs(n[FieldIndex.Y] - y) < 0.0001
                ]

                if len(collisions) == 0:
                    __active = True
                    print("activating, checking if sea", x, y)

                    if is_seapoint(Point(x, y), data):
                        print("new seapoint found", x, y)

                        add_candidate(
                            x,
                            y,
                            edgepoint[FieldIndex.INDEX],
                            edgepoint[FieldIndex.X],
                            edgepoint[FieldIndex.Y],
                            edgepoint[-1],  #length
                            end_point,
                            con,
                            travelled_factor)

                    else:
                        add_to_ground(x, y, con)
                else:  # allowing for multiple candidates for the same spot
                    candidate_collisions = [
                        n for n in collisions
                        if n[FieldIndex.STATUS] == PointStatus.CANDIDATE
                    ]
                    if len(candidate_collisions) != 0 and not any([
                            n[FieldIndex.PREVIOUS] == edgepoint[
                                FieldIndex.INDEX] for n in candidate_collisions
                    ]):
                        length = edgepoint[-1] + geo_distance(
                            (x, y),
                            (edgepoint[FieldIndex.X], edgepoint[FieldIndex.Y]))
                        heuristics = get_heuristic(x, y, length, end_point, travelled_factor)

                        if heuristics < min([
                                n[FieldIndex.HEURISTICS]
                                for n in candidate_collisions
                        ]):
                            print("adding new candidate with known",
                                  collisions)
                            print("from index", edgepoint[FieldIndex.INDEX])
                            add_candidate(
                                x,
                                y,
                                edgepoint[FieldIndex.INDEX],
                                edgepoint[FieldIndex.X],
                                edgepoint[FieldIndex.Y],
                                edgepoint[-1],  # length
                                end_point,
                                con,
                                travelled_factor)

        if not __active:
            print("inactivating edgepoint with neighbors count of",
                  len(known_neighbors))
            inactivate(edgepoint[FieldIndex.INDEX], con)
            #STOP = True

    #### checking of someone hit the target

    winner = find_winner_route(end_point, con, step)
    if len(winner) != 0:
        print("reached the target")
        return 1

    expand_best(con)
    return 0


def expand_best(con):
    """
    Making the most promising candidates new edgepoints. 
    """
    
    ## Expanding
    con.execute(
        '''
                        UPDATE points
                        SET status = ?
                        WHERE heuristics IN
                        (SELECT heuristics
                            FROM points
                            WHERE status = ?
                            GROUP BY x,y
                            ORDER BY heuristics
                            LIMIT 5
                            )
                            ''', (PointStatus.EDGE, PointStatus.CANDIDATE))

    ## Cleaning up alternaive candidates for points that are now edges
    con.execute(
        '''
                    DELETE FROM points
                    WHERE status = ? AND (x,y) IN
                    (SELECT x,y
                        FROM points
                        WHERE status = ?
                        )
                        ''', (PointStatus.CANDIDATE, PointStatus.EDGE))
    #con.execute(''' DELETE FROM points WHERE status=?''', (PointStatus.CANDIDATE,))


def get_heuristic(x, y, length, target_point, travelled_factor=1):
    """
    A function that directs the growth.
    Lower is better.
    travelled_factor regulates how important for search is path already traversed.
    1 is a setting for optimal path search. 
    Lower values make search go faster towards the goal at the cost of some optimality.
    """

    to_target = geo_distance((x, y), target_point)
    heuristic_score = to_target + length*travelled_factor
    #    heuristic_score = to_target + length/1.1
    #print("heuristics report:", x, y, length, heuristic_score)
    return heuristic_score


def route(start_point, end_point, step, travelled_factor=1):
    """
    Main usable function. \n
    Points are in format [long,lat] \n
    Step is in degrees (of longitude/latitude).
    travelled_factor regulates how important for search is path already traversed.
    1 is a setting for optimal path search. 
    Lower values make search go faster towards the goal at the cost of some optimality.
    """

    ## validate start and end points

    for p in [start_point, end_point]:
        if not is_seapoint(tuple_2_shapely(p), data):
            print("start/end point", p, "is not a sea point!")
            return []

    ## if line of sight -> route is just 2 points - start and end.
    print("checking for los")
    if has_los(start_point, end_point, radians(step) * EARTH_RADIUS * 1000):
        print ("has los, no routing needed")
        return route_to_gpx([start_point, end_point])

    ## route
    with init_db() as con:
        #prime the process
        add_to_edge(start_point, -1, end_point, con)

        while True:
            res = route_step(start_point, end_point, con, step, travelled_factor)
            if res == 1:
                route = find_winner_route(end_point, con, step)
                simplify_route(route, step)
                return route_to_gpx(route)


def route_to_gpx(array):
    """
    Array is of points - tuples or arrays [x,y]
    Output is a gpx route
    """

    gpx_route = GPX.GPXRoute(name="autorouted route")

    for point in array:
        gpx_route.points.append(
            GPX.GPXRoutePoint(latitude=point[1], longitude=point[0]))

    return gpx_route


def find_winner_route(end_point, con, step):
    """
    Finding out which route actually won by analysing circumference of an endpoint
    """

    neighbors = get_known_neighbors(end_point, con, step)
    if len(neighbors) == 0:
        return []
    winner = neighbors[0]
    route = []
    index = winner[FieldIndex.INDEX]

    while True:
        if index == -1:
            break
        point, index = point_from_index(index, con)
        route.append(point)
    route.reverse()
    route.append(end_point)
    return route


############## LOAD WATER POLYGONS######################################

## libpath is used to find files relative to the module's directory.
## Which can be wherever
    
base_path = Path(__file__).parent
shapefile_path = (base_path /
                  "./natural_earth_sea/ne_10m_bathymetry_L_0.shp").resolve()

data = geopandas.read_file(shapefile_path)  #WGS projection.
## Geopandas actually can reproject if needed

############### Graphical Debugging Interface #####################
if __name__ == '__main__':

    ################ THERE SHOULD BE CLI (EVENTUALLY)
    pass