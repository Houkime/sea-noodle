# SeaNoodle

A basic marine routing engine based on A\* and Natural Earth's public domain bathymetry data.

https://www.naturalearthdata.com/downloads/10m-physical-vectors/10m-bathymetry/

Constructs a marine route from A to B with respect to shoreline and spherical geometry

## Usage:
Currently provides Python3 API:


```
#!/usr/bin/python3

import seanoodle.seanoodle as seanoodle

start_point = [155,50]
end_point = [157,51]
route = seanoodle.route(start_point,end_point,0.02) #last argument is a step in degrees
## returns a gpxpy route.
print (route)
```

## Hacking and development:

For a quick visual demo of the algorithm itself use debug_ui.py.  
It is a convenient script for step-by-step visualisation of pathfinding progression.  
Comes in handy when playing with and/or debugging the algorithm.  